//#include "crypto_hash.h"
//#define CRYPTO_BYTES 64
#include "jh_sse2_opt64.h"
     
int crypto_hash_jh(unsigned char *out,const unsigned char *in,unsigned long long inlen)
{
      if (Hash_jh(64 * 8,in,inlen * 8,out) == SUCCESS) return 0;
      return -1;
}
