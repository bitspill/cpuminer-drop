/**
* ==========================(LICENSE BEGIN)============================
*
* Copyright (c) 2015 kernels10
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* ===========================(LICENSE END)=============================
*
* @file     lphash.c
* @author   kernels10 <kernels10@gmail.com.com>
*/

#include "cpuminer-config.h"
#include "miner.h"

#include <string.h>
#include <stdint.h>

//#include "sph_simd.h"
#include "x5/vect128/nist.h"
//------------------------------------

//#include "sph_blake.h"
#include "x6/blake.c"
//-------------------------------

#ifdef OS_DARWIN
#include "sph_groestl.h"
#else
#include "x6/grso.c"
#include "x6/grso-asm.c"
#endif
//-------------------------------

#include "sph_jh.h"
//#include "x5/jh512/bitslice_sse2_opt64/jh_sse2_opt64.h"
//-----------------------------------------------------

//#include "sph_keccak.h"
#include "x6/keccak.c"
//-------------------------------

//#include "sph_skein.h"
#include "x6/skein.c"
//#include "sph_cubehash.h"
#include "x5/cubehash_sse2.h"
//---------------------------------

#include "sph_echo.h"
#include "sph_fugue.h"

//#include "sph_luffa.h"--------------------
#include "x5/luffa_for_sse2.h"
//------------------------------------------

#include "sph_shavite.h"

/*define data alignment for different C compilers*/
#if defined(__GNUC__)
      #define DATA_ALIGN16(x) x __attribute__ ((aligned(16)))
#else
      #define DATA_ALIGN16(x) __declspec(align(16)) x
#endif

static void shiftr_lp(const uint32_t *input, uint32_t *output, unsigned int shift)
{
    if(!shift) {
        memcpy(output, input, 64);
        return;
    }

    memset(output, 0, 64);
    int i;
    for(i = 0; i < 15; ++i) {
        output[i + 1] |= (input[i] >> (32 - shift));
        output[i] |= (input[i] << shift);
    }

    output[15] |= (input[15] << shift);
    return;
}

static void switchHash(const void *input, void *output, int id)
{
    //--------------------------------------
	DATA_ALIGN16(unsigned char hashbuf[128]);
    DATA_ALIGN16(size_t hashptr);
    DATA_ALIGN16(sph_u64 hashctA);
    DATA_ALIGN16(sph_u64 hashctB);
    DATA_ALIGN16(unsigned char hash[128]);
    /* proably not needed */
    memset(hash, 0, 128);
	
	//sph_keccak512_context ctx_keccak;
    sph_blake512_context ctx_blake;
   
#ifdef OS_DARWIN
    sph_groestl512_context ctx_groestl;
#else
     grsoState sts_grs;
#endif

	//-----------------------------
	
    sph_skein512_context ctx_skein;
	
    //sph_luffa512_context ctx_luffa;
      hashState			 ctx_luffa;
   //-----------------------------------
   
    sph_echo512_context ctx_echo;
	
  //  sph_simd512_context ctx_simd;
      hashState_sd *     ctx_simd1;
	//---------------------------------
	
   // sph_cubehash512_context ctx_cubehash;
   cubehashParam		 ctx_cubehash;
   //------------------------------------------
   
    sph_fugue512_context ctx_fugue;
    sph_shavite512_context ctx_shavite;

    switch(id) {
    case 0:
       // sph_keccak512_init(&ctx_keccak); sph_keccak512(&ctx_keccak, input, 64); sph_keccak512_close(&ctx_keccak, output);
       {memcpy(hash, input, 64);
  	    DECL_KEC;
		KEC_I;
		KEC_U;
		KEC_C;
		asm volatile ("emms");
		memcpy(output, hash, 64);}
	   break;
    case 1:
       // sph_blake512_init(&ctx_blake); sph_blake512(&ctx_blake, input, 64); sph_blake512_close(&ctx_blake, output);
    {memcpy(hash, input, 64);
	DECL_BLK;
    BLK_I;
    BLK_U;
    BLK_C;
	asm volatile ("emms");
	memcpy(output, hash, 64);}
	  break;
    case 2:
       // sph_groestl512_init(&ctx_groestl); sph_groestl512(&ctx_groestl, input, 64); sph_groestl512_close(&ctx_groestl, output);
#ifdef OS_DARWIN
        sph_groestl512_init(&ctx_groestl); sph_groestl512(&ctx_groestl, input, 64); sph_groestl512_close(&ctx_groestl, output);
#else
        {memcpy(hash, input, 64);
          GRS_I;
          GRS_U;
          GRS_C;
          asm volatile ("emms");
          memcpy(output, hash, 64);}
#endif

	   break;
    case 3:
       //  sph_skein512_init(&ctx_skein); sph_skein512(&ctx_skein, input, 64); sph_skein512_close(&ctx_skein, output);
      {memcpy(hash, input, 64);
	  DECL_SKN;
	  SKN_I;
	  SKN_U;
	  SKN_C;
	  asm volatile ("emms");
	  memcpy(output, hash, 64);}
	  break;
    case 4:
      //  sph_luffa512_init(&ctx_luffa); sph_luffa512(&ctx_luffa, input, 64); sph_luffa512_close(&ctx_luffa, output);
      init_luffa(&ctx_luffa,512);
	  update_luffa(&ctx_luffa,(const BitSequence*)input,512);
	  final_luffa(&ctx_luffa,(BitSequence*)output);
	   break;
    case 5:
        sph_echo512_init(&ctx_echo); sph_echo512(&ctx_echo, input, 64); sph_echo512_close(&ctx_echo, output);
        break;
    case 6:
        sph_shavite512_init(&ctx_shavite); sph_shavite512(&ctx_shavite, input, 64); sph_shavite512_close(&ctx_shavite, output);
        break;
    case 7:
        sph_fugue512_init(&ctx_fugue); sph_fugue512(&ctx_fugue, input, 64); sph_fugue512_close(&ctx_fugue, output);
        break;
    case 8:
       // sph_simd512_init(&ctx_simd); sph_simd512(&ctx_simd, input, 64); sph_simd512_close(&ctx_simd, output);
       //-------simd512 vect128 --------------	
        ctx_simd1=malloc(sizeof(hashState_sd));
        Init(ctx_simd1,512);
        Update(ctx_simd1,(const BitSequence *)input,512);
        Final(ctx_simd1,(BitSequence *)output);
        free(ctx_simd1->buffer);
        free(ctx_simd1->A);
        free(ctx_simd1);
	   break;
    case 9:
       // sph_cubehash512_init(&ctx_cubehash); sph_cubehash512(&ctx_cubehash, input, 64); sph_cubehash512_close(&ctx_cubehash, output);
        cubehashInit(&ctx_cubehash,512,16,32);
        cubehashUpdate(&ctx_cubehash,(const byte*)input,64);
        cubehashDigest(&ctx_cubehash,(byte*)output);
        break;
    default:
        break;
    }
}

static void lphash(void *state, const void *input) {
    sph_jh512_context ctx_jh;
    uint32_t hash[2][16] __attribute__((aligned(64)));
    uint32_t *hashA = hash[0];
    uint32_t *hashB = hash[1];

    sph_jh512_init(&ctx_jh);
    sph_jh512(&ctx_jh, input, 80);
    sph_jh512_close(&ctx_jh, (void*)(hashA));
    //  hashState_jh state_jh;
    //      Init_jh(&state_jh, 64*8);
    //       Update_jh(&state_jh,(const BitSequence *)input, 640);
    //       Final_jh(&state_jh,(BitSequence *)hashA);

    unsigned int startPosition = hashA[0] % 31;
    int i = 0;
    int j = 0;
    int start = 0;

    for (i = startPosition; i < 31; i+=9) {
        start = i % 10;
        for (j = start; j < 10; j++) {
            shiftr_lp(hashA, hashB, (i & 3));
            switchHash((const void*)hashB, (void*)hashA, j);
        }
        for (j = 0; j < start; j++) {
            shiftr_lp(hashA, hashB, (i & 3));
            switchHash((const void*)hashB, (void*)hashA, j);
        }
    }
    for (i = 0; i < startPosition; i += 9) {
        start = i % 10;
        for (j = start; j < 10; j++) {
            shiftr_lp(hashA, hashB, (i & 3));
            switchHash((const void*)hashB, (void*)hashA, j);
        }
        for (j = 0; j < start; j++) {
            shiftr_lp(hashA, hashB, (i & 3));
            switchHash((const void*)hashB, (void*)hashA, j);
        }
    }

    memcpy(state, hashA, 32);
}

//static unsigned int calcPoK(uint32_t *pdata, const char *txs)
//{
//    CBlockHeader header = this->GetBlockHeader();
//    header.nVersion = header.nVersion & (~POK_DATA_MASK);

//    // The first block of this hash will be the same from nonce to nonce, so if it is ever
//    // profitable/worth it, miners can implement a midstate similar to the sha256 midstate
//    olduint::uint512 hashTxChooser = HashLP(BEGIN(header.nVersion), END(header.nNonce));
//    unsigned int nDeterRand1 = hashTxChooser.getinnerint(0);

//    if (!IsPoKBlock())
//    {
//        return nDeterRand1 & POK_DATA_MASK;
//    }

//    assert(vtx.size() > 0);

//    unsigned int nDeterRand2 = hashTxChooser.getinnerint(1);
//    unsigned int nDeterRand3 = hashTxChooser.getinnerint(2);

//    unsigned int nTxIndex =  nDeterRand1 % vtx.size();

//    const std::vector<unsigned char> * pvTxData = NULL;
//    if (pmapTxSerialized != NULL)
//    {
//        MapTxSerialized::const_iterator it = pmapTxSerialized->find(std::make_pair(this->hashMerkleRoot, nTxIndex));
//        if (it != pmapTxSerialized->end())
//        {
//            pvTxData = &(it->second);
//        }
//    }

//    std::vector<unsigned char> vTxData2;
//    if (pmapTxSerialized == NULL || pvTxData == NULL)
//    {
//        CDataStream ss(SER_NETWORK, PROTOCOL_VERSION);
//        ss << vtx[nTxIndex];
//        vTxData2.insert(vTxData2.end(), ss.begin(), ss.end());

//        if (pmapTxSerialized != NULL)
//        {
//            pmapTxSerialized->insert(std::make_pair(std::make_pair(this->hashMerkleRoot, nTxIndex), vTxData2));
//        }
//    }

//    if (pvTxData == NULL)
//        pvTxData = &vTxData2;

//    assert((pvTxData->end() - pvTxData->begin()) >= 4);

//    unsigned int nDeterRandIndex = nDeterRand2 % (pvTxData->size() - 3);
//    unsigned int nRandTxData = *((unsigned int *)(&(pvTxData->begin()[nDeterRandIndex])));

//    unsigned int nPoK = (nRandTxData ^ nDeterRand3) & POK_DATA_MASK;

//    return nPoK;
//}


//static bool IsPoKBlock(const uint32_t *pdata) {
//    return pdata[0] & POK_BOOL_MASK;
//}

//static void SetPoKFlag(uint32_t *pdata, bool fPoK) {
//    pdata[0] &= (~POK_BOOL_MASK);
//    if (fPoK)
//        pdata[0] |= POK_BOOL_MASK;
//}

//static unsigned int GetPoK(const uint32_t *pdata) {
//    return pdata[0] & POK_DATA_MASK;
//}

//static void setPoK(uint32_t *pdata, unsigned int nPoK) {
//    pdata[0] &= (~POK_DATA_MASK);
//    pdata[0] |= (POK_DATA_MASK & nPoK);
//}

int scanhash_lp(int thr_id, uint32_t *pdata, const uint32_t *ptarget, uint32_t max_nonce, unsigned long *hashes_done) {

    uint32_t hash[16] __attribute__((aligned(64)));
    uint32_t tmpdata[20] __attribute__((aligned(64)));
    const uint32_t version = pdata[0] & (~POK_DATA_MASK);
    const uint32_t first_nonce = pdata[19];
    uint32_t nonce = first_nonce;
    memcpy(tmpdata, pdata, 80);
    const uint32_t htarg = ptarget[7];

    do {
        tmpdata[0]  = version;
        tmpdata[19] = nonce;
        lphash(hash, tmpdata);
        tmpdata[0] = version | (hash[0] & POK_DATA_MASK);
        lphash(hash, tmpdata);

        if (hash[7] <= htarg && fulltest(hash, ptarget)) {
            pdata[0] = tmpdata[0];
            pdata[19] = nonce;
            *hashes_done = pdata[19] - first_nonce + 1;
            if (opt_debug)
                applog(LOG_INFO, "found nonce %x", nonce);
            return 1;
        }
        nonce++;
    } while (nonce < max_nonce && !work_restart[thr_id].restart);

    pdata[19] = nonce;
    *hashes_done = pdata[19] - first_nonce + 1;
    return 0;
}
